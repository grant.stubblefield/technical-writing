#!/usr/bin/env ruby
# frozen_string_literal: true

# Automatically create an issue based on the TW milestone planning template.
# Make sure to add the GITLAB_API_PRIVATE_TOKEN in the project's CI/CD variables.
#

require 'json'
require 'yaml'
require 'net/http'
require 'date'
require 'gitlab'

COLOR_CODE_RESET = "\e[0m"
COLOR_CODE_RED = "\e[31m"
COLOR_CODE_GREEN = "\e[32m"
RELEASE_DATES_URI = URI('https://gitlab.com/gitlab-org/gitlab-docs/-/raw/main/content/release_dates.json')
DRY_RUN = ENV['DRY_RUN'] == 'true'

#
# Fetch information about GitLab releases.
#
def release_dates
  uri = URI('https://gitlab.com/gitlab-com/www-gitlab-com/-/raw/master/data/releases.yml')
  response = Net::HTTP.get_response(uri)
  return '[]' unless response.is_a?(Net::HTTPSuccess)

  parsed_yaml = YAML.safe_load(response.body) || []
  JSON.generate(parsed_yaml)
rescue StandardError => e
  warn("Error getting release dates - #{e}")
  '[]'
end

# TODO: This currently is based on when the script is run, as it calculates
# the upcoming milestone by adding a month to the current date. For example,
# if it's run in October, the next milestone that is returned is November.
# We should make this more robust by comparing the date this is run with the
# milestone's date defined in releases.yml.
def upcoming_milestone(release_date = (Date.today >> 1).strftime('%Y-%m'))
  @upcoming_milestone ||= begin
    # Search in the relase dates hash for this month's release date
    # and fetch the milestone version number.
    releases = JSON.parse(release_dates)
    releases.find { |item| item['date'].start_with?(release_date) }&.fetch('version', nil)
  end
end

template = File.read('.gitlab/issue_templates/tw-milestone-plan.md')
assign_milestone = "/milestone %\"#{upcoming_milestone}\""
description = template + assign_milestone
issue_title = "Technical Writing milestone plan for #{upcoming_milestone}"

puts "#{COLOR_CODE_GREEN}INFO: Technical Writing milestone issue title:#{COLOR_CODE_RESET} #{issue_title}"
puts "#{COLOR_CODE_GREEN}INFO: Technical Writing milestone issue description: #{COLOR_CODE_RESET}\n"
puts description
puts "#{COLOR_CODE_GREEN}INFO: End of description.#{COLOR_CODE_RESET}\n"

# Take the project ID from the CI_PROJECT_ID predefined variable
project_id = ENV.fetch('CI_PROJECT_ID', nil)

unless DRY_RUN
  gl = Gitlab.client(endpoint: 'https://gitlab.com/api/v4', private_token: ENV.fetch('GITLAB_API_PRIVATE_TOKEN', nil))
end

if DRY_RUN
  puts "#{COLOR_CODE_GREEN}INFO: Not creating Technical Writing milestone plan issue...#{COLOR_CODE_RESET}"
else
  puts "#{COLOR_CODE_GREEN}INFO: Creating Technical Writing milestone plan issue...#{COLOR_CODE_RESET}"
  gl.create_issue(project_id, issue_title, { description: description })
end
