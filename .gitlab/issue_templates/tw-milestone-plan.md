This issue provides a consolidated view of what the Technical Writing team expects to work on with their assigned groups for the upcoming milestone. In addition to the work with their assigned groups, the [Technical Writing team](https://handbook.gitlab.com/handbook/product/ux/technical-writing/) also works on other tasks that are not reflected in this issue.

**On the `1st` of each month:**

The [TW Bot](https://gitlab.com/project_10614162_bot) creates the issue for the upcoming milestone using a [scheduled pipeline](https://gitlab.com/gitlab-org/technical-writing/-/pipeline_schedules).

We open this issue on the 1st to align with planning activities carried out by other teams and stakeholders. The 1st is on or around 12 days before the milestone begins. For example, for 16.8 (2024-01-17), the milestone starts on 2023-12-16 and we open this issue on 2023-12-01. For more information, see the [product development timeline](https://about.gitlab.com/handbook/engineering/workflow/#product-development-timeline).

**By the start of the milestone:**

The TW carries out [planning activities](https://handbook.gitlab.com/handbook/product/ux/technical-writing/workflow/#planning) for their assigned groups, and populates [Checklist and links](#checklist-and-links) to confirm these activities have taken place:

1. Have a conversation - synchronously or asynchronously - with either the group **PM** or **EM** to review:

   - Planned ~"documentation" and ~"UI text" work for the upcoming milestone (`Milestone: Upcoming`) or (`Milestone: Started`).
   - Improvement or development opportunities they're aware of in their areas of the product documentation.
   - If any deprecations can be announced according to the [process for deprecating and removing a feature](https://about.gitlab.com/handbook/product/gitlab-the-product/#process-for-deprecating-and-removing-a-feature).
     Deprecations should be announced as soon as possible, but the group should have a firm idea of deprecated features
     from about half-way through a major release cycle.

   This can be a regular group meeting or any other means of communication that you have established with your group.

   If the TW, PM, or EM is unavailable around the beginning of the milestone, we encourage using async methods (like commenting in issues or Slack messages) or having conversations at an earlier time. If the conversation happens before the creation of the issue, just add the planning information to the issue when it's available.

1. Identify a linked artifact that describes the upcoming planned work, such as:

   - An issue board for the group's documentation/UI text items for the milestone.
   - A filter that describes/lists the planned work. ([Example](https://gitlab.com/groups/gitlab-org/-/boards/3168664?scope=all&label_name[]=documentation&milestone_title=%23upcoming) for ~"devops::plan")
   - The group's planning issue, if it identifies issues/MRs that require Technical Writing team involvement. ([Example](https://gitlab.com/gitlab-org/ci-cd/pipeline-authoring/-/issues/30))

1. Select the appropriate checkboxes and share the links to planned features to confirm that the "conversation" has taken place.

## Checklist and links

After you've reviewed the plan with your PM or EM, select the checkbox for your group. If you can't meet with the PM or EM for some reason (perhaps they're on PTO), append that information to the end of the line and leave the checkbox cleared.

### Stage ~"devops::manage"

- [ ] ~"group::import and integrate" - `Link to board or filter`
- [ ] ~"group::foundations" - `Link to board or filter`

### Stage ~"devops::plan"

- [ ] ~"group::project management" -
      [board](https://gitlab.com/groups/gitlab-org/-/boards/3168664?label_name[]=documentation&milestone_title=Upcoming) |
      `planning issue`
- [ ] ~"group::product planning" - [board](https://gitlab.com/groups/gitlab-org/-/boards/3168664?label_name[]=documentation&milestone_title=Upcoming) |
      `planning issue`
- [ ] ~"group::optimize" - `Link to board or filter`

### Stage ~"devops::create"

- [ ] ~"group::code creation" - `Link to board or filter`, `planning issue`
- [ ] ~"group::code review" - [board](https://gitlab.com/groups/gitlab-org/-/boards/2159734?label_name%5B%5D=group%3A%3Acode%20review&label_name[]=Technical%20Writing), `planning issue`
- [ ] ~"group::editor extensions" - [board](https://gitlab.com/groups/gitlab-org/-/boards/7088820?label_name[]=group%3A%3Aeditor%20extensions&label_name[]=Technical%20Writing), `planning issue`
- [ ] ~"group::ide" - `Link to board or filter`
- [ ] ~"group::source code" - [board](https://gitlab.com/groups/gitlab-org/-/boards/1626594?label_name%5B%5D=group%3A%3Asource%20code&label_name[]=Technical%20Writing), `planning issue`

### Stage ~"devops::verify"

- [ ] ~"group::pipeline authoring" - `Link to board or filter`
- [ ] ~"group::pipeline execution" - `Link to board or filter`
- [ ] ~"group::pipeline security" - `Link to board or filter`
- [ ] ~"group::runner" - `Link to board or filter`
- [ ] ~"group::runner saas" - `Link to board or filter`

### Stage ~"devops::package"

- [ ] ~"group::package registry" - `Link to board or filter`
- [ ] ~"group::container registry" - `Link to board or filter`

### Stage ~"devops::deploy"

- [ ] ~"group::environments" - `Link to board or filter`

### Stage ~"devops::monitor"

- [ ] ~"group::analytics instrumentation" - `Link to board or filter`
- [ ] ~"group::product analytics" - `Link to board or filter`

### Stage ~"devops::secure"

- [ ] ~"group::static analysis" - `Link to board or filter`
- [ ] ~"group::dynamic analysis" - `Link to board or filter`
- [ ] ~"group::composition analysis" - `Link to board or filter`
- [ ] ~"group::vulnerability research" - `Link to board or filter`

### Stage ~"devops::govern"

- [ ] ~"group::security policies" - `Link to board or filter`
- [ ] ~"group::threat insights" - `Link to board or filter`
- [ ] ~"group::compliance" - `Link to board or filter`
- [ ] ~"group::anti-abuse" - `Link to board or filter`
- [ ] ~"group::authentication" - `Link to board or filter`
- [ ] ~"group::authorization" - `Link to board or filter`

### Stage ~"devops::fulfillment"

All Fulfillment groups: [board](https://gitlab.com/groups/gitlab-org/-/boards/4978071?milestone_title=Upcoming&label_name%5B%5D=documentation)

- [ ] ~"group::provision" - `Link to board or filter`
- [ ] ~"group::utilization" - `Link to board or filter`

### Stage ~"devops::systems"

- [ ] ~"group::distribution::build" / ~"group::distribution::deploy" - `Link to board or filter`
- [ ] ~"group::geo" - `Link to board or filter`
- [ ] ~"group::gitaly" - `Link to board or filter`

### Stage ~"devops::data stores"

- [ ] ~"group::cloud connector" - `Link to board or filter`
- [ ] ~"group::global search" - `Link to board or filter`
- [ ] ~"group::tenant scale" - `Link to board or filter`

### Stage ~"devops::platforms"

- [ ] ~"group::dedicated" - `Link to board or filter`

### Stage ~"devops::ai-powered"

- [ ] ~"group::ai framework" - `Link to board or filter`
- [ ] ~"group::ai model validation" - `Link to board or filter`
- [ ] ~"group::custom models" - `Link to board or filter`
- [ ] ~"group::duo chat" - `Link to board or filter`

### Stage ~"devops::modelops"

- [ ] ~"group::dataops" - `Link to board or filter`
- [ ] ~"group::mlops" - `Link to board or filter`

## Other events and team efforts this milestone

- [ ] Managers (@kpaizee, @dianalogan) check the [Developer Evangelism calendar](https://handbook.gitlab.com/handbook/marketing/developer-relations/developer-evangelism/#i-classfa-fa-calendar-aria-hiddentruei-calendar) for Hackathons and other events that may impact team velocity this milestone. List the events and dates, and let the team know in the `tw-team` Slack channel, and as a read-only in the TW Team meeting.

### Events

- `Link to events and team efforts (hackathons, other events).`

### :scissors: UX Paper Cuts

Epic that links to ~"UX Paper Cuts" team planning issue for this milestone. Issues and MRs have regular stage/group labels.

- <https://gitlab.com/groups/gitlab-org/-/epics/10026+>

### TW recurring tasks

- [Release Post structural check](https://handbook.gitlab.com/handbook/marketing/blog/release-posts/managers/): `Technical Writer`
- [Monthly doc version](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/doc/releases.md): `Technical Writer`
- [Docs project maintenance tasks](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/.gitlab/issue_templates/tw-monthly-tasks.md): `Technical Writer`
- [Review docs-only backlog issues](https://handbook.gitlab.com/handbook/product/ux/technical-writing/workflow/#review-and-triage-docs-only-backlog-issues)

### TW team KRs

- `Links to team KRs`
- `Links to team KRs`

### Other team KRs

List of KRs from other teams with ~"Technical Writing" impact or involvement this quarter:

| Stage and group | TW | KR |
| ------ | ------ | ------ |
| `stage and group label`  | `TW name` | `link to KR`|
| `stage and group label`  | `TW name` | `link to KR`|
| `stage and group label`  | `TW name` | `link to KR`|

### Docs site work

- [Issue board](https://gitlab.com/groups/gitlab-org/-/boards/4340643?label_name%5B%5D=Category%3ADocs%20Site) (ongoing work)

/assign @aqualls @rdickenson @axil @kpaizee @marcel.amirault @sselhorn @eread @msedlakjakubowski @susantacker @fneill @dianalogan @phillipwells @ashrafkhamis @jglassman1 @lciutacu @lyspin
/label ~"Technical Writing" ~"tw-plan"
