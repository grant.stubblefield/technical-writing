<!-- The issue name should be in the form: TW group change tasks for [stage]/[group] -->
<!-- Create separate issues for each significant group or stage change. -->
<!-- Link relevant issues where the change was announced -->
<!-- Link relevant MRs where the change was announced -->

Use this template to manage changes when there is a new stage/group, a deprecated stage/group, or when a group moves to a different stage.

- Stage/group change: `from group name` => `to group name`
- Stable counterpart technical writer(s) affected:
- Technical writing manager affected:
- Group change expected to be complete by: `milestone`

## Tasks for assigned technical writer

- In the `www-gitlab-com` repository:
  - [ ] Check that an update for [`data/stages.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/data/stages.yml) is in progress, and link the MR to this issue. A group or stage change is usually initiated by a Product Manager or Engineering Manager, who is responsible for following the process for [Changes](https://handbook.gitlab.com/handbook/product/categories/#changes). It the PM or EM's responsibility to raise the MR to update `stages.yml`.
  - [ ] Once the `stages.yml` MR is merged, check that the changes are reflected in [Assignments to DevOps Stages and Groups](https://handbook.gitlab.com/handbook/product/ux/technical-writing/#designated-technical-writers). The assignments table automatically pulls in the data from `stages.yml`. There should be no additional action required.
- In the `gitlab` repository, update as needed (in the following order, but can be done in the same MR):
  1. [ ] The metadata in all related markdown files.
  1. [ ] Group assignments in [`lib/tasks/gitlab/tw/codeowners.rake`](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/tasks/gitlab/tw/codeowners.rake).
  1. [ ] [`.gitlab/CODEOWNERS`](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/CODEOWNERS) by running the [`tw:codeowners` Rake task](https://docs.gitlab.com/ee/development/documentation/metadata.html#batch-updates-for-tw-metadata) to ensure no pages are missed.
- [ ] In the [Technical Writing](https://gitlab.com/gitlab-org/technical-writing) project, update the [Milestone Planning](https://gitlab.com/gitlab-org/technical-writing/-/blob/main/.gitlab/issue_templates/tw-milestone-plan.md) template.

## Tasks for technical writing manager

- [ ] Update the [TW Tableau dashboard](https://10az.online.tableau.com/#/site/gitlab/workbooks/2367394/views).
- [ ] Update google sheets used for team rebalancing planning.
- [ ] Check Workday for the Job Title Specialty (Multi-Select). If the specialty does not exist, create an issue in the People Group [general project](https://gitlab.com/gitlab-com/people-group/people-tools-technology/general), and use the **Workday: Job Title Specialty Request** template.
- [ ] Update [Workday](https://handbook.gitlab.com/handbook/people-group/workday-guide/) with the new group assignments. Workday updates the group assignments in `data/team_members/person`. Interim instructions during Workday rollout: Manager emails `people-connect@gitlab.com` with details of changes.

/label ~"Technical Writing"

