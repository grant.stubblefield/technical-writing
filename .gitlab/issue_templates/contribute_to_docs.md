<!-- Use this template to help GitLab team members get set up to contribute to documentation.-->

## Familiarize yourself with how GitLab authors documentation

Read the following to become familiar with how technical writing is done at GitLab:
- [ ] Complete the [GitLab Technical Writing Fundamentals](https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals/technical-writing-fundamentals/) course. 
- [ ] Read [GitLab Documentation guidelines](https://docs.gitlab.com/ee/development/documentation/index.html)
     and the various pages linked from the introduction.
- [ ] Familiarize yourself with [the word list](https://docs.gitlab.com/ee/development/documentation/styleguide/word_list.html) and [style guide](https://docs.gitlab.com/ee/development/documentation/styleguide/).
- [ ] Look at [the `gitlab` project's `doc` directory](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc). Each page on the docs site is in this directory. For example:
  - This page on the docs site: https://docs.gitlab.com/ee/user/project/labels.html
  - Corresponds to this page in the repo: https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/project/labels.md

    For quick access to any markdown file, when you're on the docs site, scroll to the bottom and select **View page source**.
- [ ] Review [the GitLab docs navigation file](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/content/_data/navigation.yaml). This file is how the left navigation on the docs site is populated.

## Set up your computer

Prerequisites:
- You must have at least the Developer role in the [gitlab](https://gitlab.com/gitlab-org/gitlab) project.

- [ ] Ensure you have Git installed and [your SSH key added to GitLab](https://docs.gitlab.com/ee/ssh/#add-an-ssh-key-to-your-gitlab-account).
- [ ] Install an IDE, like VS Code.
- [ ] Clone the GitLab repo to your local machine, and view the files in VS Code.
- [ ] Optional. Set up a local docs build on your machine by following [these instructions](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/doc/setup.md).

Now you can start creating small merge requests with doc updates.

## Create a merge request (MR)

You can create an MR many different ways. Some possibilities are:

- Open a markdown file in the repo and in the top right, select **Edit** (to edit in the IDE or in the UI).
- Go to the page on the docs site, scroll to the bottom, and **View Page Source**. Then Edit in the top right.
- In your local environment, in the repo you cloned, edit the markdown file. You can also use this method to create a new markdown file. Then:
  1. Create a branch: `git checkout -b mybranchname`
  1. Add the files you added or edited: `git add .`
  1. Add a commit message: `git commit -m "My commit message here"` (Start with a capital letter, use 3-5 words, do not end in period)
  1. Push to the repo: `git push origin mybranchname`
  1. After you run this command, follow the URL Git gives you to create the MR.

To get a review, ask the #docs slack channel or assign the MR to a writer by using [roulette](https://gitlab-org.gitlab.io/gitlab-roulette/?sortKey=stats.avg30&order=-1&hourFormat24=true&visible=maintainer%7Cdocs).
